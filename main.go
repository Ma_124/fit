package main

import (
	"context"
	"os"
	"strings"

	"./api"
	"github.com/c-bata/go-prompt"
	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/file"
	"github.com/kballard/go-shellquote"
	"gopkg.in/alecthomas/kingpin.v2"
	"gopkg.in/src-d/go-git.v4"
)

var (
	app = kingpin.New("fit", "A git client that fits you!")

	appInteractive = app.Command("interactive", "Run an interactive fit shell.").Alias("i")

	human             = app.Command("human", "Turns URL into a human-friendly directory name.").Hidden()
	humanPreserveCase = human.Flag("presv-case", "Preserves the case of the directory name. Useful as a display name or for case-insensitive systems (e.g. Windows)").Short('c').Bool()
	humanRepo         = human.Arg("repo", "The repository URL to convert.").Required().String()

	gitignores   = app.Command("gitignores", "Automatically generates a .gitignore").Alias("ignore")
	gitignorests = gitignores.Arg("gitignores", "The .gitignore templates to load from $GITIGNORE_PATH or github.com/github/gitignore (','-separated list of template names)").String()

	initc       = app.Command("init", "Initializes a git repository.")
	initRepo    = initc.Arg("repository", "Origin repository.").String()
	initIgnores = initc.Flag("gitignores", "The .gitignore templates to load from $GITIGNORE_PATH or github.com/github/gitignore (','-separated list of template names)").Short('i').String()

	clone          = app.Command("clone", "Clone a repository from git.")
	cloneRepo      = clone.Arg("repository", "The repository to clone.").Required().String()
	cloneDir       = clone.Arg("directory", "The name of a new directory to clone into.").String()
	recurseSubMods = clone.Flag("submod-depth", "The submodule rescursivity.").Short('s').Default("10").Uint()

	ls = app.Command("ls", "List files")
)

type Config struct {
	User struct {
		Name string
		Mail string
	}
	Gitignores string
	Servers    []struct {
		Domain string
		Name   string
	}
}

func main() {
	var err error

	cfg := &Config{}

	loader := confita.NewLoader(
		file.NewBackend(os.Getenv("HOME") + "/.fit.yml"),
	)

	err = loader.Load(context.Background(), cfg)
	if err != nil {
		kingpin.Fatalf("An error occurred.\n%s\n", err.Error())
	}

	api.SetAuthor(cfg.User.Name, cfg.User.Mail)

	err = Execute(kingpin.MustParse(app.Parse(os.Args[1:])), cfg)

	if err != nil {
		kingpin.Fatalf("An error occurred.\n%s\n", err.Error())
	}
}

func Execute(cmd string, cfg *Config) error {
	var err error
	var repo *git.Repository
	switch cmd {
	case "init":
		repo, err = api.Init()
		if err != nil {
			return err
		}
		if *initRepo != "" {
			api.AddRemote(repo, *initRepo, "origin")
		}

		err = Gitignore(cfg, *initIgnores)
		break
		// TODO "new" (interactive init (go-promt))
		// TODO "ls" (ls-files + color)
		// TODO "license"
		// TODO "commit" alias "c"
		// TODO "push" alias "p"
		// TODO "commitpush"? alias "cp" (commit + push)
	case "clone":
		if *cloneDir == "" {
			*cloneDir = api.GetDirNameFromURL(*cloneRepo)
		}
		repo, err = api.Clone(*cloneRepo, *cloneDir, *recurseSubMods)
		break
	case "human":
		println(api.GetHumanNameFromURL(*humanRepo, *humanPreserveCase))
		break
	case "gitignores":
		err = Gitignore(cfg, *gitignorests)
		break
	case "ls":
		repo, err = Repo()
		api.LS(repo)
		break
	case "interactive":
		for _, s := range cfg.Servers {
			servers = append(servers, prompt.Suggest{Text: "https://" + s.Domain + "/", Description: s.Name + " (HTTPS)"}, prompt.Suggest{Text: "git@" + s.Domain + ":", Description: s.Name + " (SSH)"})
		}

		for true {
			inp := strings.TrimSpace(prompt.Input("> ", completer, promptOptions...))
			if inp == "exit" {
				break
			}

			args, err := shellquote.Split(inp)
			if err != nil {
				handleErr(err)
			}

			if len(args) == 0 {
				continue
			}

			inp, err = app.Parse(args)
			if err != nil {
				handleErr(err)
				continue
			}

			err = Execute(inp, cfg)
			handleErr(err)
		}
		break
	}
	return err
}
