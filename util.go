package main

import (
	"strings"

	"./api"
	"gopkg.in/src-d/go-git.v4"
)

func Gitignore(cfg *Config, ignores string) error {
	return api.Gitignore(append(strings.Split(ignores, ","), strings.Split(cfg.Gitignores, ",")...))
}

func Repo() (*git.Repository, error) {
	fs, s, err := api.GetRepositoryFileSystems(".")
	if err != nil {
		return nil, err
	}
	return git.Open(s, fs)
}
