package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/c-bata/go-prompt"
	"github.com/kballard/go-shellquote"
)

var promptOptions = []prompt.Option{
	prompt.OptionTitle("Fit - A git client that fits you!"),
	prompt.OptionPrefixTextColor(prompt.Blue),
	prompt.OptionInputTextColor(prompt.LightGray),
}

var commands = []prompt.Suggest{
	{Text: "exit", Description: "Exit the interactive fit shell."},
	{Text: "init", Description: "Initializes a git repository."},
	{Text: "new", Description: "Creates a git repository."},
	{Text: "clone", Description: "Clone a repository from git."},
	{Text: "gitignores", Description: "Automatically generates a .gitignore."},
	{Text: "human", Description: "Turns URL into a human-friendly directory name."},
}

var servers []prompt.Suggest

var flagsHuman = []prompt.Suggest{
	{Text: "--presv-case", Description: "Preserves the case of the directory name. Useful as a display name or for case-insensitive systems (e.g. Windows, HFS)"},
}

var flagsInit = []prompt.Suggest{
	{Text: "--gitignores", Description: "The .gitignore templates to load from $GITIGNORE_PATH or github.com/github/gitignore (','-separated list of template names)"}, // TODO Issue#1
}

var flagsClone = []prompt.Suggest{
	{Text: "--submod-depth", Description: "The submodule rescursivity."}, // TODO Issue#1
}

func completer(d prompt.Document) []prompt.Suggest {
	if !strings.ContainsRune(d.TextBeforeCursor(), ' ') {
		return prompt.FilterHasPrefix(commands, d.TextBeforeCursor(), false)
	}

	args, err := shellquote.Split(d.TextBeforeCursor())
	if err != nil {
		handleErr(err)
		return []prompt.Suggest{}
	}
	w := d.GetWordBeforeCursor()
	_ = len(args) - 1

	if strings.HasPrefix(w, "-") {
		return prompt.FilterHasPrefix(optionCompleter(args[0]), w, false)
	}

	return prompt.FilterHasPrefix(argCompleter(args[0]), w, true)
}

func optionCompleter(cmd string) []prompt.Suggest {
	switch cmd {
	case "init":
		return flagsInit
	case "clone":
		return flagsClone
	case "human":
		return flagsHuman
	}
	return []prompt.Suggest{}
}

func argCompleter(cmd string) []prompt.Suggest {
	switch cmd {
	case "init":
		fallthrough
	case "clone":
		fallthrough // TODO Issue#2
	case "human":
		return servers
	}
	return []prompt.Suggest{}
}

func handleErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "An error occurred.\n%s\n", err.Error())
	}
}
