package api

import (
	"strings"

	"runtime"

	"gopkg.in/src-d/go-billy.v4"
	"gopkg.in/src-d/go-billy.v4/osfs"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/storage/filesystem"
)

func GetRepositoryFileSystems(path string) (billy.Filesystem, *filesystem.Storage, error) {
	fs := osfs.New(path)
	gfs, err := fs.Chroot(".git")
	if err != nil {
		return fs, nil, err
	}
	s, err := filesystem.NewStorage(gfs)
	return fs, s, err
}

func GetHumanNameFromURL(url string, preserveCase bool) string {
	ps := strings.Split(url, "/")
	n := ps[len(ps)-1]
	if strings.HasSuffix(n, ".git") {
		n = n[:len(n)-4]
	}
	if preserveCase {
		return n
	}
	return strings.ToLower(n)
}

func GetDirNameFromURL(url string) string {
	switch runtime.GOOS {
	case "windows":
		return GetHumanNameFromURL(url, true)
	default:
		return GetHumanNameFromURL(url, false)
	}
}

func LS(repo *git.Repository) ([]git.FileStatus, error) {
	w, err := repo.Worktree()
	if err != nil {
		return nil, err
	}

	ss, err := w.Status()
	if err != nil {
		return nil, err
	}

	fs, err := w.Filesystem.ReadDir(".")
	if err != nil {
		return nil, err
	}

	for _, f := range fs {
		println(f.Name())
	}
	ss = ss
	return nil, nil
}
