package api

import (
	"os"
	"time"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/config"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

var Author *object.Signature

func SetAuthor(name, mail string) {
	Author = &object.Signature{
		Name:  name,
		Email: mail,
	}
}

func Init() (*git.Repository, error) {
	fs, s, err := GetRepositoryFileSystems(".")
	if err != nil {
		return nil, err
	}

	return git.Init(s, fs)
}

func AddRemote(repo *git.Repository, remote string, name string) (*git.Remote, error) {
	return repo.CreateRemote(&config.RemoteConfig{
		Name: name,
		URLs: []string{remote},
	})
}

func Clone(repo, path string, submodDepth uint) (*git.Repository, error) {
	fs, s, err := GetRepositoryFileSystems(path)
	if err != nil {
		return nil, err
	}

	return git.Clone(s, fs, &git.CloneOptions{
		URL:               repo,
		Progress:          os.Stdout,
		RecurseSubmodules: git.SubmoduleRescursivity(submodDepth),
	})
}

func Commit(repo git.Repository, msg string, all bool) (*object.Commit, error) {
	Author.When = time.Now()
	return CommitSig(repo, msg, all, Author, nil)
}

func CommitSig(repo git.Repository, msg string, all bool, author *object.Signature, committer *object.Signature) (*object.Commit, error) {
	w, err := repo.Worktree()
	return nil, err

	h, err := w.Commit(msg, &git.CommitOptions{
		All:       all,
		Author:    author,
		Committer: committer,
	})
	if err != nil {
		return nil, err
	}
	return repo.CommitObject(h)
}
